<?php

namespace Drupal\tacjs_extras\Form\Steps;

/**
 * Class AddServices
 */
class AddServices extends \Drupal\tacjs\Form\Steps\AddServices
{
  /**
   * @return array
   */
  protected function getContent()
  {
    $content = parent::getContent();

    // Add missing defaults tacjs services to the "Add Services" settings form
    $content['api']['googlemapsembed'] = [
      "about" => [
        "name" => "Google Maps (embed)",
        "privacy" => "https://www.google.fr/intl/policies/privacy/",
      ],
      "code" => [
        "js" => "",
        "html" => "<div class=\"googlemapsembed\" data-url=\"###URL###\" width=\"###WIDTH###\" height=\"###HEIGHT###\" ></div>",
      ],
      "cookies" => [
        "apisid",
        "hsid",
        "nid",
        "sapisid",
        "sid",
        "sidcc",
        "ssid",
        "1p_jar",
      ],
      "detection" => [
        "domain" => "www.google.com/maps",
        "tracker" => "<iframe width=\"###WIDTH###\" height=\"###HEIGHT###\" frameborder=\"0\" style=\"border:0\" src=\"###URL###\" allowfullscreen> </iframe>",
      ],
    ];

    // Allow other tacjs modules to add their custom services to the "Add Services" settings form
    \Drupal::moduleHandler()->invokeAll('tacjs_services_add', [ &$content ]);

    return $content;
  }

}
