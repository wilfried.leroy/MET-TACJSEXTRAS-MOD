/**
 * @file
 * Init tarteaucitron.js script.
 */

(function($, Drupal, drupalSettings, tarteaucitron) {

  /**
   * Trigger custom jQuery "loaded" event for TacJS custom services initialization
   */
  $(document).trigger('tacjs_extras:tacjs_loaded', [ tarteaucitron ]);

  /**
   * Init tarteaucitron.js script.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the script.
   */
  Drupal.behaviors.tacjs = {
    attach: function attach() {
      $(document).once('tacjs_extras').each(function () {
        // Add custom texts to tarteaucitron.
        window.tarteaucitronCustomText = drupalSettings.tacjs.texts;

        // Set expiration time in day(s)
        if (drupalSettings.tacjs.expire) {
          window.tarteaucitronExpireInDay = true;
          window.tarteaucitronForceExpire = drupalSettings.tacjs.expire;
        }

        if (drupalSettings.tacjs.user) {
          // Workaround for #3120915: convert multiplegtagUa into a Array
          // See https://github.com/AmauriC/tarteaucitron.js/pull/394
          if (drupalSettings.tacjs.user.multiplegtagUa &&
            typeof drupalSettings.tacjs.user.multiplegtagUa === "string") {
            drupalSettings.tacjs.user.multiplegtagUa = drupalSettings.tacjs.user.multiplegtagUa.split(",");
          }

          // Add users identification to tarteaucitron.
          tarteaucitron.user = drupalSettings.tacjs.user;
        }

        // Add enabled services to tarteaucitron services.
        Object.keys(drupalSettings.tacjs.services)
          .filter(function(service) {
            // Filter enabled services.
            return drupalSettings.tacjs.services[service];
          })
          .forEach(function(service) {
            (tarteaucitron.job = tarteaucitron.job || []).push(service);
          });

        // Disable consent proof logging if not available
        if (drupalSettings.tacjs.disableLog) {
          tarteaucitron.store = function () {};
        }

        // Initiate tarteaucitron
        tarteaucitron.init(drupalSettings.tacjs.dialog);
        if (document.readyState === 'complete') {
          tarteaucitron.initEvents.loadEvent(!window.addEventListener);
        }
      });
    }
  };
})(jQuery, Drupal, drupalSettings, tarteaucitron);
