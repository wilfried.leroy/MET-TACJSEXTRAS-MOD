// comarquage, public procedures servicepublic.fr
tarteaucitron.services.comarquage = {
  "key": "comarquage",
  "type": "api",
  "name": "Comarquage (Guide des démarches administratives)",
  "needConsent": true,
  "cookies": [],
  "js": function () {
    "use strict";
    // when user allows cookies
    tarteaucitron.fallback(['tac_comarquage'], function (x) {
      var dataId = x.getAttribute('data-id');
      var dataPath = x.getAttribute('data-path');
      comarquage.container.init({ parentUrl: "/", updateUrl: true });
      comarquage.container.renderCosmetic3Gadget({
        id: dataId, viewParams: { path: dataPath, twoColumns: false, target: "_blank" }, width: "100%"
      });
      return '';
    });
  },
  "fallback": function () {
    "use strict";
    var serviceKey = 'comarquage';
    tarteaucitron.fallback(['tac_comarquage'], function (elem) {
      return tarteaucitron.engage(serviceKey);
    });
  }
};

// dailymotion juke box
tarteaucitron.services.dailymotionjukebox = {
  "key": "dailymotionjukebox",
  "type": "video",
  "name": "Dailymotion juke box (Galerie de vidéos)",
  "needConsent": true,
  "cookies": ['CONSENT', '_ga', '_gid', 'tzo'],
  "js": function () {
    "use strict";
    // when user allows cookies
    tarteaucitron.fallback(['tac_dailymotionjukebox'], function (x) {
      var dataId = x.getAttribute("data-id");
      var dataAllowfullscreen = x.getAttribute("data-allowfullscreen");
      var dataAllowtransparency = x.getAttribute("data-allowtransparency");
      var dataStyle = x.getAttribute("data-style");
      var dataWidth = x.getAttribute("data-width");
      var dataAlign = x.getAttribute("data-align");
      var dataFrameborder = x.getAttribute("data-frameborder");
      var dataMarginwidth = x.getAttribute("data-marginwidth");
      var dataMarginheight = x.getAttribute("data-marginheight");
      var dataSrc = x.getAttribute("data-src");
      return '<iframe id="' + dataId + '" allowfullscreen="' + dataAllowfullscreen + '" allowtransparency="' + dataAllowtransparency + '" style="' + dataStyle + '" width="' + dataWidth + '" align="' + dataAlign + '" frameborder="' + dataFrameborder + '" marginwidth="' + dataMarginwidth + '" marginheight="' + dataMarginheight + '" src="' + dataSrc + '"></iframe>';
    });
  },
  "fallback": function () {
    "use strict";
    var serviceKey = 'dailymotionjukebox';
    tarteaucitron.fallback(['tac_dailymotionjukebox'], function (elem) {
      return tarteaucitron.engage(serviceKey);
    });
  }
};

// dailymotion widget
tarteaucitron.services.dailymotionwidget = {
  "key": "dailymotionwidget",
  "type": "video",
  "name": "Dailymotion widget (Lecteur vidéos)",
  "needConsent": true,
  "cookies": ['e', 'dmxId', 'damd', 'dm-euconsent-v2', 'rtk_session', 'dmvk', 'lang', 'ff', 'UID', '_gid', 'ts', '_ga', 'client_token', 'v1st', 'usprivacy'],
  "js": function () {
    "use strict";
    // when user allows cookies
    (function(w,d,s,u,n,e,c){w.PXLObject = n; w[n] = w[n] || function(){(w[n].q =w[n].q || []).push(arguments);};w[n].l = 1 * new Date();e = d.createElement(s); e.async = 1; e.src = u;c = d.getElementsByTagName(s)[0]; c.parentNode.insertBefore(e,c);});
    tarteaucitron.addScript('//api.dmcdn.net/pxl/client.js', 'pxl');
    tarteaucitron.fallback(['tac_dailymotionwidget'], function (x) {
      var dataPlacement = x.getAttribute("data-placement");
      return '<div class="dailymotion-widget" data-placement="' + dataPlacement + '"> </div>';
    });
  },
  "fallback": function () {
    "use strict";
    var serviceKey = 'dailymotionwidget';
    tarteaucitron.fallback(['tac_dailymotionwidget'], function (elem) {
      return tarteaucitron.engage(serviceKey);
    });
  }
};

// lightwidget, Instagram images gallery
tarteaucitron.services.lightwidget = {
  "key": "lightwidget",
  "type": "api",
  "name": "Lightwidget (Galerie d'images Instagram)",
  "needConsent": true,
  "cookies": ['__cfduid'],
  "js": function () {
    "use strict";
    // when user allows cookies
    tarteaucitron.addScript('https://cdn.lightwidget.com/widgets/lightwidget.js');
    tarteaucitron.fallback(['tac_lightwidget'], function (x) {
      var dataSrc = x.getAttribute('data-src');
      var dataScrolling= x.getAttribute('data-scrolling');
      var dataAllowtransparency= x.getAttribute('data-allowtransparency');
      var dataClass= x.getAttribute('data-class');
      var dataStyle= x.getAttribute('data-style');
      return '<iframe src="' + dataSrc + '" scrolling="' + dataScrolling + '" allowtransparency="' + dataAllowtransparency + '" class="' + dataClass + '" style="' + dataStyle + '"></iframe>';
    });
  },
  "fallback": function () {
    "use strict";
    var serviceKey = 'lightwidget';
    tarteaucitron.fallback(['tac_lightwidget'], function (elem) {
      return tarteaucitron.engage(serviceKey);
    });
  }
};

// mailperformance, newletters inscriptions service
tarteaucitron.services.mailperformance = {
  "key": "mailperformance",
  "type": "other",
  "name": "NP6 (Inscription aux infolettres)",
  "needConsent": true,
  "cookies": ['__hssc', '__hssrc', 'hubspotutk', '__hstc', '_fbp', '_gid', '_ga'],
  "js": function () {
    "use strict";
    // when user allows cookies
    tarteaucitron.fallback(['tac_mailperformance'], function (x) {
      var dataSrc = x.getAttribute('data-src');
      var dataWidth = x.getAttribute('data-width');
      var dataHeight = x.getAttribute('data-height');
      var dataFrameborder = x.getAttribute('data-frameborder');
      var dataScrolling = x.getAttribute('data-scrolling');
      return '<iframe src="' + dataSrc + '" width="' + dataWidth + '" height="' + dataHeight + '" frameborder="' + dataFrameborder + '" scrolling="' + dataScrolling + '"></iframe>'
    });
  },
  "fallback": function () {
    "use strict";
    var serviceKey = 'mailperformance';
    tarteaucitron.fallback(['tac_mailperformance'], function (elem) {
      return tarteaucitron.engage(serviceKey);
    });
  }
};

// viewsurf
tarteaucitron.services.viewsurf = {
  "key": "viewsurf",
  "type": "video",
  "name": "Viewsurf (Lecteur vidéos)",
  "needConsent": true,
  "cookies": ['SERVERID43093', 'SRV', 'symfony'],
  "js": function () {
    "use strict";
    // when user allows cookies
    tarteaucitron.fallback(['tac_viewsurf'], function (x) {
      var dataSrc = x.getAttribute('data-src');
      var dataFrameborder = x.getAttribute("data-frameborder");
      var dataScrolling= x.getAttribute('data-scrolling');
      var dataAllowfullscreen= x.getAttribute('data-allowfullscreen');
      var dataStyle= x.getAttribute('data-style');
      return '<iframe src="' + dataSrc + '" frameborder="' + dataFrameborder + '" scrolling="' + dataScrolling + '" allowfullscreen="' + dataAllowfullscreen + '" style="' + dataStyle + '"></iframe>';
    });
  },
  "fallback": function () {
    "use strict";
    var serviceKey = 'viewsurf';
    tarteaucitron.fallback(['tac_viewsurf'], function (elem) {
      return tarteaucitron.engage(serviceKey);
    });
  }
};

// Old Xiti
tarteaucitron.services.oldxiti = {
  "key": "oldxiti",
  "type": "analytic",
  "name": "Xiti (AT Internet)",
  "uri": "https://helpcentre.atinternet-solutions.com/hc/fr/categories/360002439300-Privacy-Centre",
  "needConsent": true,
  "cookies": [ "xtvrn" ],
  "js": function () {
    "use strict";
    if (tarteaucitron.user.oldxitiParams === undefined
      || tarteaucitron.user.oldxitiParams.xtcoreurl === undefined) {
      return;
    }
    window.xtnv = tarteaucitron.user.oldxitiParams.xtnv;
    window.xtsd = tarteaucitron.user.oldxitiParams.xtsd;
    window.xtsite = tarteaucitron.user.oldxitiParams.xtsite;
    window.xtn2 = tarteaucitron.user.oldxitiParams.xtn2;
    window.xtpage = tarteaucitron.user.oldxitiParams.xtpage;
    window.xtdi = tarteaucitron.user.oldxitiParams.xtdi;
    var xt_ac = tarteaucitron.user.oldxitiParams.xt_ac;
    var xt_an = tarteaucitron.user.oldxitiParams.xt_an;
    var xt_multc = tarteaucitron.user.oldxitiParams.xt_multc;
    if (xt_ac !== undefined && xt_an !== undefined && xt_multc !== undefined) {
      if (window.xtparam != null) {
        window.xtparam += "&ac=" + xt_ac + "&an="+ xt_an + xt_multc;
      } else {
        window.xtparam = "&ac=" + xt_ac + "&an=" + xt_an + xt_multc;
      }
    }
    tarteaucitron.addScript(tarteaucitron.user.oldxitiParams.xtcoreurl, '', function() {});
  }
};

// oldaddthis
tarteaucitron.services.oldaddthis = {
  "key": "oldaddthis",
  "type": "social",
  "name": "AddThis",
  "uri": "https://www.addthis.com/privacy/privacy-policy#publisher-visitors",
  "needConsent": true,
  "cookies": ['__atuvc', '__atuvs'],
  "js": function () {
    "use strict";
    if (tarteaucitron.user.oldaddthisPubId === undefined) {
      return;
    }
    tarteaucitron.fallback(['addthis_toolbox'], function (elem) {
      var dataAddThis = elem.parentNode.getAttribute("data-addthis");
      return dataAddThis == null ? elem.innerHTML : dataAddThis;
    });
    tarteaucitron.addScript('//s7.addthis.com/js/250/addthis_widget.js#pubid=' + tarteaucitron.user.oldaddthisPubId);
  },
  "fallback": function () {
    "use strict";
    var serviceKey = 'oldaddthis';
    tarteaucitron.fallback(['addthis_toolbox'], function (elem) {
      elem.parentNode.setAttribute("data-addthis", elem.innerHTML); // backup buttons html in li parent element
      return tarteaucitron.engage(serviceKey);
    });
  }
};
