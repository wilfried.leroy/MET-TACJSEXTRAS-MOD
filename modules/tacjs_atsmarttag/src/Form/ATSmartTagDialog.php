<?php

namespace Drupal\tacjs_atsmarttag\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ManageDialog.
 *
 * @package Drupal\tacjs\Form
 */
class ATSmartTagDialog extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tacjs_atsmarttag_dialog';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['tacjs_atsmarttag.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('tacjs_atsmarttag.settings');

    $form['collect_domain'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Serveur de collecte'),
      '#default_value' => $config->get('stats_collect_domain'),
    );
    $form['collect_domain_ssl'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Serveur de collecte (SSL)'),
      '#default_value' => $config->get('stats_collect_domain_ssl'),
    );
    $form['site'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Identifiant du site'),
      '#default_value' => $config->get('stats_site'),
    );
    $form['cnil_exempt'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Exemption CNIL'),
      '#default_value' => $config->get('stats_cnil_exempt') === NULL ? 1 : $config->get('stats_cnil_exempt'),
    );
    $form['need_consent'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Consentement requis'),
      '#default_value' => $config->get('stats_need_consent') === NULL ? 1 : $config->get('stats_need_consent'),
    );
    $form['required'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Obligatoire'),
      '#description' => $this->t('Attention ! Retire le service de Tarteaucitron.'),
      '#default_value' => $config->get('stats_required'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('tacjs_atsmarttag.settings');

    $config
      ->set('stats_collect_domain', $form_state->getValue('collect_domain'))
      ->set('stats_collect_domain_ssl', $form_state->getValue('collect_domain_ssl'))
      ->set('stats_site', $form_state->getValue('site'))
      ->set('stats_cnil_exempt', $form_state->getValue('cnil_exempt'))
      ->set('stats_need_consent', $form_state->getValue('need_consent'))
      ->set('stats_required', $form_state->getValue('required'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
