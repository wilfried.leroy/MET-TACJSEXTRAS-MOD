/**
 * MixCloud Tarteaucitron service
 */
(function ($, Drupal, drupalSettings, tarteaucitron) {
  // MixCloud
  tarteaucitron.services.mixcloud = {
    "key": "mixcloud",
    "type": "video",
    "name": "MixCloud (Podcast)",
    "needConsent": true,
    "cookies": ['__cfduid'],
    "js": function () {
      "use strict";
      tarteaucitron.fallback(['tac_mixcloud'], function (x) {
        var dataSrc = x.getAttribute('data-src');
        var dataWidth= x.getAttribute('data-width');
        var dataHeight= x.getAttribute('data-height');
        var iframeHTML = '<iframe src="' + dataSrc + '" width="' + dataWidth + '" height="' + dataHeight + '" frameBorder="0"';
        if (x.getAttribute('data-autoplay') === 'true') {
          iframeHTML += ' allow="autoplay"';
        }
        iframeHTML += '></iframe>';
        return iframeHTML;
      });
    },
    "fallback": function () {
      "use strict";
      var serviceKey = 'mixcloud';
      tarteaucitron.fallback(['tac_mixcloud'], function (elem) {
        elem.style.width = elem.getAttribute("data-width") + "px";
        elem.style.height = elem.getAttribute("data-height") + "px";
        return tarteaucitron.engage(serviceKey);
      });
    }
  };
})(jQuery, Drupal, drupalSettings, tarteaucitron);
