<?php

namespace Drupal\tacjs_mixcloud\Element;

use Drupal\bm_mixcloud\Element\AudioEmbedMixCloud;

/**
 * Providers an element design for embedding MixCloud audio content.
 *
 * @RenderElement("audio_embed_mixcloud_tac")
 */
class AudioEmbedMixCloudTAC extends AudioEmbedMixCloud {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#theme' => 'audio_embed_mixcloud_tac',
      '#audioid' => '',
      '#width' => '',
      '#height' => '',
      '#autoplay' => FALSE,
      '#attributes' => [],
      '#pre_render' => [
        [static::class, 'preRenderEmbed'],
      ],
    ];
  }

  /**
   * Transform the render element structure into a renderable one.
   *
   * @param array $element
   *   An element array before being processed.
   *
   * @return array
   *   The processed and renderable element.
   */
  public static function preRenderEmbed($element) {
    $element['#src'] = 'https://www.mixcloud.com/widget/iframe/?hide_cover=1&feed=' . urlencode($element['#audioid']);
    if (isset($element['#autoplay']) && $element['#autoplay']) {
      $element['#autoplay'] = 'true';
    }
    return $element;
  }
}



