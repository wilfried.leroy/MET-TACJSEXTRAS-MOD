<?php

namespace Drupal\tacjs_mixcloud\Plugin\audio_embed_field\Provider;

use Drupal\bm_mixcloud\Plugin\audio_embed_field\Provider\MixCloud;

/**
 * A Tarteaucitron compatible MixCloud provider plugin.
 */
class MixCloudTAC extends MixCloud {

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    if (!\Drupal::service('router.admin_context')->isAdminRoute()) {
      $embed_code = [
        '#type' => 'audio_embed_mixcloud_tac',
        '#audioid' => $this->getAudioId(),
        '#width' => $width,
        '#height' => $height,
        '#autoplay' => $autoplay,
      ];
    } else {
      $embed_code = parent::renderEmbedCode($width, $height, $autoplay);
    }
    return $embed_code;
  }
}
