Domain TacJS Extras module
==========================

* This module allows configuration of Tarteaucitron
  services on a multi-domain Drupal 8 web site.

Configuration:
--------------
Domain TacJS Extras module have configuration setting page
to set tag configuration against each Domains.

Configuration page path:
/admin/config/domain/tacjs_extras_domain

Dependencies:
-------------
    -domain
    -tacjs_extras
