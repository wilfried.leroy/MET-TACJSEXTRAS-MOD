<?php

namespace Drupal\tacjs_extras_domain\Form\Steps;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\tacjs_extras\Form\Steps\AddServices;

/**
 * Class DomainServicesSettingsForm.
 *
 * @package Drupal\tacjs_extras_domain\Form
 */
class DomainAddServices extends AddServices {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    $domain_id = $this->getRequest()->get('domain_id');
    return [
      'tacjs_extras_domain.' . $domain_id . '.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tacjs_extras_domain_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $domain_id = $this->getRequest()->get('domain_id');
    $config = $this->config('tacjs_extras_domain.' . $domain_id . '.settings');
    if ($config->isNew()) {
      $config->merge($this->config('tacjs.settings')->getRawData());
    }

    $form['domain_id'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Domain ID'),
      '#default_value' => $domain_id,
    ];

    $form['domain'] = [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => t('Domain: @domain_id', [ '@domain_id' => $domain_id ]),
      '#weight' => -100,
    ];

    $form['services'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Add Services'),
      '#title_display' => 'invisible',
    ];

    foreach ($this->content as $type => $services) {
      $form['service']['type_' . $type] = [
        '#type' => 'details',
        '#title' => Unicode::ucfirst($type),
        '#group' => 'services',
      ];

      foreach ($services as $service => $value) {
        $form['service']['type_' . $type]['service_' . $service] = [
          '#type' => 'checkbox',
          '#title' => $value['about']['name'],
          '#default_value' => $config->get('services.' . $service),
        ];

        if (preg_match_all('/tarteaucitron.user.([A-z]+)[\s]+=[\s]+[\\]?[\"]?[\']?###([^.]+)###/m', $value['code']['js'] ?: (is_array($value['code']['html']) ? '' : $value['code']['html']), $match)) {
          for ($i = 0; $i < count($match[1]); $i++) {
            $form['service']['type_' . $type]['user_' . $match[1][$i]] = [
              '#type' => 'textfield',
              '#title' => $match[1][$i],
              '#default_value' => $config->get('user.' . $match[1][$i]),
              '#title_display' => 'invisible',
              '#attributes' => [
                'placeholder' => $match[2][$i],
              ],
              '#states' => [
                'visible' => [
                  ':input[name="service_' . $service . '"]' => ['checked' => TRUE],
                ],
              ],
            ];
          }
        }
      }
    }

    // Workaround for #3170835: Uncaught TypeError null user
    // See https://github.com/AmauriC/tarteaucitron.js/pull/394
    $form['service']['type_analytic']['user_multiplegtagUa']['#attributes']['placeholder'] = 'UA-XXXXXXXX-X,UA-XXXXXXXX-X,UA-XXXXXXXX-X';

    return ConfigFormBase::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $domain_id = $form_state->getValue('domain_id');
    $config = $this->config('tacjs_extras_domain.' . $domain_id . '.settings');
    if ($config->isNew()) {
      $config->merge($this->config('tacjs.settings')->getRawData());
    }

    foreach ($this->content as $type => $services) {
      foreach ($services as $service => $value) {
        $config->set('services.' . $service, $form_state->getValue('service_' . $service));

        if (preg_match_all('/tarteaucitron.user.([A-z]+)[\s]+=[\s]+[\\]?[\"]?[\']?###([^.]+)###/m', $value['code']['js'] ?: (is_array($value['code']['html']) ? '' : $value['code']['html']), $match)) {
          for ($i = 0; $i < count($match[1]); $i++) {
            if ($form_state->getValue('user_' . $match[1][$i])) {
              $config->set('user.' . $match[1][$i], $form_state->getValue('user_' . $match[1][$i]));
            }
            else {
              $config->clear('user.' . $match[1][$i]);
            }
          }
        }
      }
    }

    $config->save();

    ConfigFormBase::submitForm($form, $form_state);
  }
}
