<?php

namespace Drupal\tacjs_extras_domain\Form\Steps;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\tacjs\Form\Steps\ManageDialog;

/**
 * Class ManageDialog.
 *
 * @package Drupal\tacjs\Form
 */
class DomainManageDialog extends ManageDialog {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tacjs_extras_domain_manage_dialog';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    $domain_id = $this->getRequest()->get('domain_id');
    return ['tacjs_extras_domain.' . $domain_id . '.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $domain_id = $this->getRequest()->get('domain_id');
    $config = $this->config('tacjs_extras_domain.' . $domain_id . '.settings');
    if ($config->isNew()) {
      $config->merge($this->config('tacjs.settings')->getRawData());
    }

    $form['domain_id'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Domain ID'),
      '#default_value' => $domain_id,
    ];

    $form['domain'] = [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => t('Domain: @domain_id', [ '@domain_id' => $domain_id ]),
      '#weight' => -100,
    ];

    $form['privacyUrl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Privacy url'),
      '#description' => $this->t('Privacy policy url'),
      '#default_value' => $config->get('dialog.privacyUrl'),
    ];

    $form['hashtag'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hashtag'),
      '#description' => $this->t('Open the panel with this hashtag'),
      '#default_value' => $config->get('dialog.hashtag'),
    ];

    $form['cookieName'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cookie name'),
      '#default_value' => $config->get('dialog.cookieName'),
      '#required' => TRUE,
    ];

    $form['orientation'] = [
      '#type' => 'radios',
      '#title' => $this->t('Orientation'),
      '#description' => $this->t('Banner position (top - bottom)'),
      '#default_value' => $config->get('dialog.orientation'),
      '#options' => [
        'top' => $this->t('Top'),
        'middle' =>  $this->t('Middle'),
        'bottom' => $this->t('Bottom'),
      ],
    ];

    $form['bodyPosition'] = [
      '#type' => 'radios',
      '#title' => $this->t('Body position'),
      '#description' => $this->t('Body position (top - bottom)'),
      '#default_value' => $config->get('dialog.bodyPosition'),
      '#options' => [
        'top' => $this->t('Top'),
        'bottom' => $this->t('Bottom'),
      ],
    ];

    $form['showAlertSmall'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show alert small'),
      '#description' => $this->t('Show the small banner on bottom right'),
      '#default_value' => $config->get('dialog.showAlertSmall'),
    ];

    $form['cookieslist'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Cookies list'),
      '#description' => $this->t('Show the cookie list'),
      '#default_value' => $config->get('dialog.cookieslist'),
    ];

    $form['showIcon'] = [
      '#type'=> 'checkbox',
      '#title' => $this->t('Show Icon'),
      '#description' => $this->t('Show cookie icon to manage cookies'),
      '#default_value' => $config->get('dialog.showIcon'),
    ];

    $form['iconPosition'] = [
      '#type' => 'radios',
      '#title' => $this->t('Icon position'),
      '#description' => $this->t('Position of the icon between BottomRight, BottomLeft, TopRight and TopLeft'),
      '#default_value' => $config->get('dialog.iconPosition'),
      '#options' => [
        'BottomRight' => $this->t('BottomRight'),
        'BottomLeft' => $this->t('BottomLeft'),
        'TopRight' => $this->t('TopRight'),
        'TopLeft' => $this->t('TopLeft'),
      ],
    ];

    $form['adblocker'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Adblocker'),
      '#description' => $this->t('Show a Warning if an adblocker is detected'),
      '#default_value' => $config->get('dialog.adblocker'),
    ];

    $form['DenyAllCta'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Deny all cta'),
      '#description' => $this->t('Show the deny all button'),
      '#default_value' => $config->get('dialog.AcceptAllCta'),
    ];


    $form['AcceptAllCta'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Accept all cta'),
      '#description' => $this->t('Show the accept all button when high privacy on'),
      '#default_value' => $config->get('dialog.AcceptAllCta'),
    ];

    $form['highPrivacy'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('High privacy'),
      '#description' => $this->t('Disable auto consent'),
      '#default_value' => $config->get('dialog.highPrivacy'),
    ];

    $form['handleBrowserDNTRequest'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Handle browser DNT request'),
      '#description' => $this->t('If Do Not Track == 1, disallow all'),
      '#default_value' => $config->get('dialog.handleBrowserDNTRequest'),
    ];

    $form['removeCredit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove credit'),
      '#description' => $this->t('Remove credit link'),
      '#default_value' => $config->get('dialog.removeCredit'),
    ];

    $form['moreInfoLink'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('More info link'),
      '#description' => $this->t('Show more info link'),
      '#default_value' => $config->get('dialog.moreInfoLink'),
    ];

    $form['useExternalCss'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use external CSS'),
      '#default_value' => $config->get('dialog.useExternalCss'),
    ];

    $form['useExternalJs'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use external JS'),
      '#default_value' => $config->get('dialog.useExternalJs'),
    ];

    $form['cookieDomain'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Cookie domain'),
      '#description' => $this->t('Shared cookie for subdomain website'),
      '#default_value' => $config->get('dialog.cookieDomain'),
    ];

    $form['readmoreLink'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Read more link'),
      '#description' => $this->t('Change the default readmore link pointing to opt-out.ferank.eu'),
      '#default_value' => $config->get('dialog.readmoreLink'),
    ];

    $form['mandatory'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Mandatory'),
      '#default_value' => $config->get('dialog.mandatory'),
    ];

    $form['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced'),
    ];

    $form['advanced']['expire'] = [
      '#type' => 'number',
      '#title' => $this->t('Expiration time'),
      '#default_value' => $config->get('expire'),
      '#field_suffix' => ' ' . t('day(s)'),
      '#min' => 1,
      '#max' => 365,
    ];

    return ConfigFormBase::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $domain_id = $this->getRequest()->get('domain_id');
    $config = $this->config('tacjs_extras_domain.' . $domain_id . '.settings');
    if ($config->isNew()) {
      $config->merge($this->config('tacjs.settings')->getRawData());
    }

    $config
      ->set('dialog.privacyUrl', $form_state->getValue('privacyUrl'))
      ->set('dialog.hashtag', $form_state->getValue('hashtag'))
      ->set('dialog.cookieName', $form_state->getValue('cookieName'))
      ->set('dialog.orientation', $form_state->getValue('orientation'))
      ->set('dialog.bodyPosition', $form_state->getValue('bodyPosition'))
      ->set('dialog.showAlertSmall', $form_state->getValue('showAlertSmall'))
      ->set('dialog.cookieslist', $form_state->getValue('cookieslist'))
      ->set('dialog.showIcon', $form_state->getValue('showIcon'))
      ->set('dialog.iconPosition', $form_state->getValue('iconPosition'))
      ->set('dialog.adblocker', $form_state->getValue('adblocker'))
      ->set('dialog.DenyAllCta', $form_state->getValue('DenyAllCta'))
      ->set('dialog.AcceptAllCta', $form_state->getValue('AcceptAllCta'))
      ->set('dialog.highPrivacy', $form_state->getValue('highPrivacy'))
      ->set('dialog.handleBrowserDNTRequest', $form_state->getValue('handleBrowserDNTRequest'))
      ->set('dialog.removeCredit', $form_state->getValue('removeCredit'))
      ->set('dialog.moreInfoLink', $form_state->getValue('moreInfoLink'))
      ->set('dialog.useExternalCss', $form_state->getValue('useExternalCss'))
      ->set('dialog.useExternalJs', $form_state->getValue('useExternalJs'))
      ->set('dialog.cookieDomain', $form_state->getValue('cookieDomain'))
      ->set('dialog.readmoreLink', $form_state->getValue('readmoreLink'))
      ->set('dialog.mandatory', $form_state->getValue('mandatory'))
      ->set('expire', $form_state->getValue('expire'))
      ->save();

    ConfigFormBase::submitForm($form, $form_state);
  }

}
