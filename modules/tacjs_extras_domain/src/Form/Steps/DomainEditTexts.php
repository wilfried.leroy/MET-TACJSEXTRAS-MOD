<?php

namespace Drupal\tacjs_extras_domain\Form\Steps;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class EditTexts.
 *
 * @package Drupal\tacjs\Form
 */
class DomainEditTexts extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tacjs_extras_domain_edit_texts';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    $domain_id = $this->getRequest()->get('domain_id');
    return ['tacjs_extras_domain.' . $domain_id . '.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $domain_id = $this->getRequest()->get('domain_id');
    $config = $this->config('tacjs_extras_domain.' . $domain_id . '.settings');
    if ($config->isNew()) {
      $config->merge($this->config('tacjs.settings')->getRawData());
    }

    $form['domain_id'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Domain ID'),
      '#default_value' => $domain_id,
    ];

    $form['domain'] = [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => t('Domain: @domain_id', [ '@domain_id' => $domain_id ]),
      '#weight' => -100,
    ];

    $form['middleBarHead'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Middle Bar Head'),
      '#default_value' => $config->get('texts.middleBarHead'),
      '#placeholder' => $this->t('☝ 🍪')
    ];

    $form['adblock'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Adblock'),
      '#default_value' => $config->get('texts.adblock'),
      '#placeholder' => $this->t('Hello! This site is transparent and lets you choose the 3rd party services you want to allow.')
    ];

    $form['adblock_call'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Adblock Call'),
      '#default_value' => $config->get('texts.adblock_call'),
      '#placeholder' => $this->t('Please disable your adblocker to start customizing.')
    ];

    $form['reload'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Reload'),
      '#default_value' => $config->get('texts.reload'),
      '#placeholder' => $this->t('Refresh the page')
    ];

    $form['alertBigScroll'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Alert Big Scroll'),
      '#default_value' => $config->get('texts.alertBigScroll'),
      '#placeholder' => $this->t('By continuing to scroll,')
    ];

    $form['alertBigClick'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Alert Big Click'),
      '#default_value' => $config->get('texts.alertBigClick'),
      '#placeholder' => $this->t('If you continue to browse this website,')
    ];

    $form['alertBig'] = [
      '#type' => 'textarea',
      '#title' => $this->t('AlertBig'),
      '#default_value' => $config->get('texts.alertBig'),
      '#placeholder' => $this->t('you are allowing all third-party services')
    ];

    $form['alertBigPrivacy'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Alert Big Privacy'),
      '#default_value' => $config->get('texts.alertBigPrivacy'),
      '#placeholder' => $this->t('This site uses cookies and gives you control over what you want to activate')
    ];

    $form['alertSmall'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Alert Small'),
      '#default_value' => $config->get('texts.alertSmall'),
      '#placeholder' => $this->t('Manage services')
    ];

    $form['personalize'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Personalize'),
      '#default_value' => $config->get('texts.personalize'),
      '#placeholder' => $this->t('Personalize')
    ];

    $form['acceptAll'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Accept All'),
      '#default_value' => $config->get('texts.acceptAll'),
      '#placeholder' => $this->t('OK, accept all')
    ];

    $form['close'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Close'),
      '#default_value' => $config->get('texts.close'),
      '#placeholder' => $this->t('Close')
    ];

    $form['privacyUrl'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Privacy Url'),
      '#default_value' => $config->get('texts.privacyUrl'),
      '#placeholder' => $this->t('Privacy policy')
    ];

    $form['all'] = [
      '#type' => 'textarea',
      '#title' => $this->t('All'),
      '#default_value' => $config->get('texts.all'),
      '#placeholder' => $this->t('Preference for all services')
    ];

    $form['info'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Info'),
      '#default_value' => $config->get('texts.info'),
      '#placeholder' => $this->t('Protecting your privacy')
    ];

    $form['disclaimer'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Disclaimer'),
      '#default_value' => $config->get('texts.disclaimer'),
      '#placeholder' => $this->t('By allowing these third party services, you accept their cookies and the use of tracking technologies necessary for their proper functioning.')
    ];

    $form['allow'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Allow'),
      '#default_value' => $config->get('texts.allow'),
      '#placeholder' => $this->t('Allow')
    ];

    $form['deny'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Deny'),
      '#default_value' => $config->get('texts.deny'),
      '#placeholder' => $this->t('Deny')
    ];

    $form['noCookie'] = [
      '#type' => 'textarea',
      '#title' => $this->t('No Cookie'),
      '#default_value' => $config->get('texts.noCookie'),
      '#placeholder' => $this->t('This service does not use cookie.')
    ];

    $form['useCookie'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Use Cookie'),
      '#default_value' => $config->get('texts.useCookie'),
      '#placeholder' => $this->t('This service can install')
    ];

    $form['useCookieCurrent'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Use Cookie Current'),
      '#default_value' => $config->get('texts.useCookieCurrent'),
      '#placeholder' => $this->t('This service has installed')
    ];

    $form['useNoCookie'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Use No Cookie'),
      '#default_value' => $config->get('texts.useNoCookie'),
      '#placeholder' => $this->t('This service has not installed any cookie.')
    ];

    $form['more'] = [
      '#type' => 'textarea',
      '#title' => $this->t('More'),
      '#default_value' => $config->get('texts.more'),
      '#placeholder' => $this->t('Read more')
    ];

    $form['source'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Source'),
      '#default_value' => $config->get('texts.source'),
      '#placeholder' => $this->t('View the official website')
    ];

    $form['credit'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Credit'),
      '#default_value' => $config->get('texts.credit'),
      '#placeholder' => $this->t('Cookies manager by tarteaucitron.js')
    ];

    $form['noServices'] = [
      '#type' => 'textarea',
      '#title' => $this->t('No Services'),
      '#default_value' => $config->get('texts.noServices'),
      '#placeholder' => $this->t('This website does not use any cookie requiring your consent.')
    ];

    $form['toggleInfoBox'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Toggle Info Box'),
      '#default_value' => $config->get('texts.toggleInfoBox'),
      '#placeholder' => $this->t('Show/hide informations about cookie storage')
    ];

    $form['title'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Title'),
      '#default_value' => $config->get('texts.title'),
      '#placeholder' => $this->t('Cookies management panel')
    ];

    $form['cookieDetail'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Cookie Detail'),
      '#default_value' => $config->get('texts.cookieDetail'),
      '#placeholder' => $this->t('Cookie detail for')
    ];

    $form['ourSite'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Our Site'),
      '#default_value' => $config->get('texts.ourSite'),
      '#placeholder' => $this->t('on our site')
    ];

    $form['newWindow'] = [
      '#type' => 'textarea',
      '#title' => $this->t('New Window'),
      '#default_value' => $config->get('texts.newWindow'),
      '#placeholder' => $this->t('(new window)')
    ];

    $form['allowAll'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Allow All'),
      '#default_value' => $config->get('texts.allowAll'),
      '#placeholder' => $this->t('Allow all cookies')
    ];

    $form['denyAll'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Deny All'),
      '#default_value' => $config->get('texts.denyAll'),
      '#placeholder' => $this->t('Deny all cookies')
    ];

    $form['fallback'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Fallback'),
      '#default_value' => $config->get('texts.fallback'),
      '#placeholder' => $this->t('is disabled.')
    ];

    $form['ads_title'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Ads Title'),
      '#default_value' => $config->get('texts.ads.title'),
      '#placeholder' => $this->t('Advertising network')
    ];

    $form['ads_details'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Ads Details'),
      '#default_value' => $config->get('texts.ads.details'),
      '#placeholder' => $this->t('Ad networks can generate revenue by selling advertising space on the site.')
    ];

    $form['analytic_title'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Analytic Title'),
      '#default_value' => $config->get('texts.analytic.title'),
      '#placeholder' => $this->t('Audience measurement')
    ];

    $form['analytic_details'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Analytic Details'),
      '#default_value' => $config->get('texts.analytic.details'),
      '#placeholder' => $this->t('The audience measurement services used to generate useful statistics attendance to improve the site.')
    ];

    $form['social_title'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Social Title'),
      '#default_value' => $config->get('texts.social.title'),
      '#placeholder' => $this->t('Social networks')
    ];

    $form['social_details'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Social Details'),
      '#default_value' => $config->get('texts.social.details'),
      '#placeholder' => $this->t('Social networks can improve the usability of the site and help to promote it via the shares.')
    ];

    $form['video_title'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Video Title'),
      '#default_value' => $config->get('texts.video.title'),
      '#placeholder' => $this->t('Videos')
    ];

    $form['video_details'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Video Details'),
      '#default_value' => $config->get('texts.video.details'),
      '#placeholder' => $this->t('Video sharing services help to add rich media on the site and increase its visibility.')
    ];

    $form['comment_title'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Comment Title'),
      '#default_value' => $config->get('texts.comment.title'),
      '#placeholder' => $this->t('Comments')
    ];

    $form['comment_details'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Comment Details'),
      '#default_value' => $config->get('texts.comment.details'),
      '#placeholder' => $this->t('Comments managers facilitate the filing of comments and fight against spam.')
    ];

    $form['support_title'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Support Title'),
      '#default_value' => $config->get('texts.support.title'),
      '#placeholder' => $this->t('Support')
    ];

    $form['support_details'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Support Details'),
      '#default_value' => $config->get('texts.support.details'),
      '#placeholder' => $this->t('Support services allow you to get in touch with the site team and help to improve it.')
    ];

    $form['api_title'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Api Title'),
      '#default_value' => $config->get('texts.api.title'),
      '#placeholder' => $this->t('APIs')
    ];

    $form['api_details'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Api Details'),
      '#default_value' => $config->get('texts.api.details'),
      '#placeholder' => $this->t('APIs are used to load scripts: geolocation, search engines, translations, ...')
    ];

    $form['other_title'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Other Title'),
      '#default_value' => $config->get('texts.other.title'),
      '#placeholder' => $this->t('Other')
    ];

    $form['other_details'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Other Details'),
      '#default_value' => $config->get('texts.other.details'),
      '#placeholder' => $this->t('Services to display web content.')
    ];

    $form['mandatoryTitle'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Mandatory title'),
      '#default_value' => $config->get('texts.mandatoryTitle'),
      '#placeholder' => $this->t('Mandatory cookies')
    ];

    $form['mandatoryText'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Mandatory text'),
      '#default_value' => $config->get('texts.mandatoryText'),
      '#placeholder' => $this->t('This site uses cookies necessary for its proper functioning which cannot be deactivated.')
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $domain_id = $this->getRequest()->get('domain_id');
    $config = $this->config('tacjs_extras_domain.' . $domain_id . '.settings');
    if ($config->isNew()) {
      $config->merge($this->config('tacjs.settings')->getRawData());
    }

    $elements = ['middleBarHead', 'adblock', 'adblock_call', 'reload', 'alertBigScroll', 'alertBigClick', 'alertBig', 'alertBigPrivacy', 'alertSmall', 'personalize', 'acceptAll', 'close', 'privacyUrl', 'all', 'info', 'disclaimer', 'allow', 'deny', 'noCookie', 'useCookie', 'useCookieCurrent', 'useNoCookie', 'more', 'source', 'credit', 'noServices', 'toggleInfoBox', 'title', 'cookieDetail', 'ourSite', 'newWindow', 'allowAll', 'denyAll', 'fallback', 'ads_title', 'ads_details', 'analytic_title', 'analytic_details', 'social_title', 'social_details', 'video_title', 'video_details', 'comment_title', 'comment_details', 'support_title', 'support_details', 'api_title', 'api_details', 'other_title', 'other_details', 'mandatoryTitle', 'mandatoryText'];

    foreach ($elements as $key) {
      if ($form_state->getValue($key)) {
        $config->set('texts.' . $key, $form_state->getValue($key));
      } else {
        $config->clear('texts.' . $key);
      }
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
