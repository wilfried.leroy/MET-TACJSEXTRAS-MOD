<?php

namespace Drupal\tacjs_extras_domain\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Class DomainSiteSettingsController.
 *
 * @package Drupal\tacjs_extras_domain\Controller
 */
class DomainServicesController extends ControllerBase {

  /**
   * Function Provide the list of modules.
   *
   * @return array
   *   Domain list.
   */
  public function domainList() {
    $domains = $this->entityTypeManager()->getStorage('domain')->loadMultipleSorted();
    $rows = [];
    /** @var \Drupal\domain\DomainInterface $domain */
    foreach ($domains as $domain) {
      $row = [
        $domain->label(),
        $domain->getCanonical(),
        Link::fromTextAndUrl($this->t('Edit'), Url::fromRoute('tacjs_extras_domain.manage_dialog', ['domain_id' => $domain->id()])),
      ];
      $rows[] = $row;
    }
    // Build a render array which will be themed as a table.
    $build['pager_example'] = [
      '#rows' => $rows,
      '#header' => [
        $this->t('Name'),
        $this->t('Hostname'),
        $this->t('Edit Settings'),
      ],
      '#type' => 'table',
      '#empty' => $this->t('No domain record found.'),
    ];
    return $build;
  }

}
