<?php

namespace Drupal\bm_mixcloud\Plugin\audio_embed_field\Provider;

use Drupal\audio_embed_field\ProviderPluginBase;

/**
 * A MixCloud provider plugin.
 *
 * @AudioEmbedProvider(
 *   id = "mixcloud",
 *   title = @Translation("MixCloud")
 * )
 */
class MixCloud extends ProviderPluginBase {

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    $embed_code = [];
    $embed_code['#type'] = 'audio_embed_mixcloud';
    $embed_code['#audioid'] = $this->getAudioId();
    $embed_code['#width'] = $width;
    $embed_code['#height'] = $height;
    $embed_code['#autoplay'] = $autoplay;
    return $embed_code;
  }

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input)
  {
    preg_match('@^https://www\.mixcloud\.com(?<id>[^"\&]+)@i', $input, $matches);
    return isset($matches['id']) ? $matches['id'] : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl()
  {
    return NULL;
  }
}
