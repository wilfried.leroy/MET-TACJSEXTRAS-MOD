<?php

namespace Drupal\bm_mixcloud\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Template\Attribute;

/**
 * Providers an element design for embedding MixCloud audio content.
 *
 * @RenderElement("audio_embed_mixcloud")
 */
class AudioEmbedMixCloud extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#theme' => 'audio_embed_mixcloud',
      '#audioid' => '',
      '#width' => '',
      '#height' => '',
      '#autoplay' => FALSE,
      '#attributes' => [],
      '#pre_render' => [
        [static::class, 'preRenderEmbed'],
      ],
    ];
  }

  /**
   * Transform the render element structure into a renderable one.
   *
   * @param array $element
   *   An element array before being processed.
   *
   * @return array
   *   The processed and renderable element.
   */
  public static function preRenderEmbed($element) {
    $element['#src'] = 'https://www.mixcloud.com/widget/iframe/?hide_cover=1&feed=' . urlencode($element['#audioid']);
    if (isset($element['#autoplay']) && $element['#autoplay']) {
      if (!is_array($element['#attributes'])) {
        $element['#attributes'] = [];
      }
      $element['#attributes']['allow'] = 'autoplay';
    }
    if (is_array($element['#attributes'])) {
      $element['#attributes'] = new Attribute($element['#attributes']);
    }
    return $element;
  }
}
