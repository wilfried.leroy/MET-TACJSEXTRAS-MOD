AT Internet SmartTags for Domain
================================

* This module allows configuration of AT Internet SmartTags
  on a multi-domain Drupal 8 web site.

Configuration:
--------------
Domain AT Internet SmartTag module have configuration setting page
to set tag configuration against each Domains.

Configuration page path:
/admin/config/domain/tacjs_atsmarttag_domain

Dependencies:
-------------
    -domain
    -tacjs_extras
