<?php

namespace Drupal\tacjs_atsmarttag_domain\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Routing\RequestContext;

/**
 * Class DomainConfigSettingsForm.
 *
 * @package Drupal\tacjs_atsmarttag_domain\Form
 */
class DomainSmartTagSettingsForm extends ConfigFormBase {

  /**
   * The path alias manager.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * The path validator.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * The request context.
   *
   * @var \Drupal\Core\Routing\RequestContext
   */
  protected $requestContext;

  /**
   * Constructs a SiteInformationForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   The path alias manager.
   * @param \Drupal\Core\Path\PathValidatorInterface $path_validator
   *   The path validator.
   * @param \Drupal\Core\Routing\RequestContext $request_context
   *   The request context.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AliasManagerInterface $alias_manager, PathValidatorInterface $path_validator, RequestContext $request_context) {
    parent::__construct($config_factory);

    $this->aliasManager = $alias_manager;
    $this->pathValidator = $path_validator;
    $this->requestContext = $request_context;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('config.factory'), $container->get('path_alias.manager'), $container->get('path.validator'), $container->get('router.request_context')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'tacjs_atsmarttag_domain.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tacjs_atsmarttag_domain_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('tacjs_atsmarttag_domain.settings');
    $domain_id = $this->getRequest()->get('domain_id');
    $tacjs_atsmarttag_config = $this->config('tacjs_atsmarttag.settings');

    $collect_domain = $tacjs_atsmarttag_config->getOriginal('stats_collect_domain', FALSE);
    if ($config->get($domain_id) != NULL) {
      $collect_domain = $config->get($domain_id . '.collect_domain');
    }
    $collect_domain_ssl = $tacjs_atsmarttag_config->getOriginal('stats_collect_domain_ssl', FALSE);
    if ($config->get($domain_id) != NULL) {
      $collect_domain_ssl = $config->get($domain_id . '.collect_domain');
    }
    $site = $tacjs_atsmarttag_config->getOriginal('stats_site', FALSE);
    if ($config->get($domain_id) != NULL) {
      $site = $config->get($domain_id . '.site');
    }
    $cnil_exempt = $tacjs_atsmarttag_config->getOriginal('stats_cnil_exempt', FALSE);
    if ($config->get($domain_id) != NULL) {
      $cnil_exempt = $config->get($domain_id . '.cnil_exempt');
    }
    $need_consent = $tacjs_atsmarttag_config->getOriginal('stats_need_consent', FALSE);
    if ($config->get($domain_id) != NULL) {
      $need_consent = $config->get($domain_id . '.need_consent');
    }
    $required = $tacjs_atsmarttag_config->getOriginal('stats_required', FALSE);
    if ($config->get($domain_id) != NULL) {
      $required = $config->get($domain_id . '.required');
    }

    $form['domain'] = [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => t('Domain: @domain_id', [ '@domain_id' => $domain_id ]),
      '#weight' => -100,
    ];

    $form['collect_domain'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Serveur de collecte'),
      '#default_value' => $collect_domain,
    );
    $form['collect_domain_ssl'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Serveur de collecte (SSL)'),
      '#default_value' => $collect_domain_ssl,
    );
    $form['site'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Identifiant du site'),
      '#default_value' => $site,
    );
    $form['cnil_exempt'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Exemption CNIL'),
      '#default_value' => $cnil_exempt === NULL ? 1 : $cnil_exempt,
    );
    $form['need_consent'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Consentement requis'),
      '#default_value' => $need_consent === NULL ? 1 : $need_consent,
    );
    $form['required'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Obligatoire'),
      '#description' => $this->t('Attention ! Retire le service de Tarteaucitron.'),
      '#default_value' => $required,
    );

    $form['domain_id'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Domain ID'),
      '#default_value' => $domain_id,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $domain_id = $form_state->getValue('domain_id');
    $collect_domain = $form_state->getValue('collect_domain');
    $collect_domain_ssl = $form_state->getValue('collect_domain_ssl');
    $site = $form_state->getValue('site');
    $cnil_exempt = $form_state->getValue('cnil_exempt');
    $need_consent = $form_state->getValue('need_consent');
    $required = $form_state->getValue('required');

    $config = $this->config('tacjs_atsmarttag_domain.settings');
    $config->set($domain_id . '.collect_domain', $collect_domain);
    $config->set($domain_id . '.collect_domain_ssl', $collect_domain_ssl);
    $config->set($domain_id . '.site', $site);
    $config->set($domain_id . '.cnil_exempt', $cnil_exempt);
    $config->set($domain_id . '.need_consent', $need_consent);
    $config->set($domain_id . '.required', $required);
    $config->save();
  }

}
