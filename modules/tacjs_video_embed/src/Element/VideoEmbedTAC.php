<?php

namespace Drupal\tacjs_video_embed\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * Providers an element design for embedding TAC DIV.
 *
 * @RenderElement("video_embed_tac")
 */
class VideoEmbedTAC extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#theme' => 'video_embed_tac',
      '#provider' => '',
      '#videoid' => '',
      '#autoplay' => '',
      '#start' => '',
      '#rel' => '',
      '#width' => '',
      '#height' => '',
      '#pre_render' => [
        [static::class, 'preRenderTACEmbed'],
      ],
    ];
  }

  /**
   * Transform the render element structure into a renderable one.
   *
   * @param array $element
   *   An element array before being processed.
   *
   * @return array
   *   The processed and renderable element.
   */
  public static function preRenderTACEmbed($element) {
    $element['#theme'] .= !empty($element['#provider']) ? '__' . $element['#provider'] : '';
    return $element;
  }
}
