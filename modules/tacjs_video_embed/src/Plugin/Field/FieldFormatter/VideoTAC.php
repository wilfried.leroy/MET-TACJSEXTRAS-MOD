<?php

namespace Drupal\tacjs_video_embed\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\video_embed_field\Plugin\Field\FieldFormatter\Video;

/**
 * Plugin implementation of the video field formatter.
 *
 * @FieldFormatter(
 *   id = "video_embed_field_video_tac",
 *   label = @Translation("Video (TAC)"),
 *   field_types = {
 *     "video_embed_field"
 *   }
 * )
 */
class VideoTAC extends Video {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = parent::viewElements($items, $langcode);
    foreach ($items as $delta => $item) {
      if (!(isset($element[$delta]['#theme']) && $element[$delta]['#theme'] == 'video_embed_field_missing_provider')) {
        if ($this->getSetting('responsive')) {
          $element[$delta]['#attached']['library'][] = 'tacjs_video_embed/responsive-video';
        }
      }
    }
    return $element;
  }
}
