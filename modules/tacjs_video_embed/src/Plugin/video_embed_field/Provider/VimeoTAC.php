<?php

namespace Drupal\tacjs_video_embed\Plugin\video_embed_field\Provider;

use Drupal\video_embed_field\Plugin\video_embed_field\Provider\Vimeo;

/**
 * A Tarteaucitron compatible Vimeo provider plugin.
 */
class VimeoTAC extends Vimeo {

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    if (!\Drupal::service('router.admin_context')->isAdminRoute()) {
      $embed_code = [
        '#type' => 'video_embed_tac',
        '#provider' => 'vimeo',
        '#videoid' => $this->getVideoId(),
        '#autoplay' => $autoplay,
        '#start' => $this->getTimeIndex(),
        '#rel' => '0',
        '#width' => $width,
        '#height' => $height,
      ];
    } else {
      $embed_code = parent::renderEmbedCode($width, $height, $autoplay);
    }
    return $embed_code;
  }
}
