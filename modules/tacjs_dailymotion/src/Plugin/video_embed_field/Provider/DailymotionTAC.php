<?php

namespace Drupal\tacjs_dailymotion\Plugin\video_embed_field\Provider;

use Drupal\video_embed_dailymotion\Plugin\video_embed_field\Provider\Dailymotion;

/**
 * A Tarteaucitron compatible Dailymotion provider plugin.
 */
class DailymotionTAC extends Dailymotion {

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    if (!\Drupal::service('router.admin_context')->isAdminRoute()) {
      $embed_code = [
        '#type' => 'video_embed_tac',
        '#provider' => 'dailymotion',
        '#videoid' => $this->getVideoId(),
        '#autoplay' => $autoplay,
        '#rel' => '0',
        '#width' => $width,
        '#height' => $height,
      ];
    } else {
      $embed_code = parent::renderEmbedCode($width, $height, $autoplay);
    }
    return $embed_code;
  }
}
